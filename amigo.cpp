#include "amigo.hpp"
#include <string>

using namespace std;

amigo::amigo(){
	setNome("Vazio");
	setIdade("Vazio");
	setTelefone("Vazio");
	setFacebook("Vazio");
	setEmail("Vazio");

}

amigo::amigo(string nome,string idade, string telefone,string facebook,string email){
	//this->nome = nome;
	//this->idade = idade;
	//this->telefone = telefone;
	//this->facebook = facebook;
	//this->email = email;
	setNome(nome);
	setIdade(idade);
	setTelefone(telefone);
	setFacebook(facebook);
	setEmail(email);
}

string amigo::getFacebook(){

	return facebook;

}
			
void amigo::setFacebook(string facebook){

    this->facebook = facebook;
}
			
string amigo::getEmail(){

	return email;

}

void amigo::setEmail(string email){

	this->email = email;
}
