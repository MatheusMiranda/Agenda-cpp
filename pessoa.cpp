#include "pessoa.hpp"
#include <string>

using namespace std;

Pessoa::Pessoa(){ 
    nome="vazio";
    telefone="vazio";
    idade="vazio";
}


Pessoa::Pessoa(string nome, string idade, string telefone){
	this->nome=nome;
	this->telefone=telefone;
	this->idade=idade;
}

			
string Pessoa::getNome(){
	return nome;
}			 

void Pessoa::setNome(string nome){
	this->nome = nome;
}

string Pessoa::getTelefone(){
	return telefone;
}			 

void Pessoa::setTelefone(string telefone){
	this->telefone = telefone;
}

string Pessoa::getIdade(){
	return idade;
}			 

void Pessoa::setIdade(string idade){
	this->idade = idade;
}
