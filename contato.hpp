#ifndef CONTATO_H
#define CONTATO_H

#include "pessoa.hpp"
#include <string>

class contato : public Pessoa{
private:string referencia;
		
public: contato();
		contato(string nome,string telefone, string idade,string referencia);
		void setReferencia(string referencia);
		string getReferencia();

};
#endif
