#ifndef PESSOA_H
#define PESSOA_H

#include <iostream>
#include <string>

using namespace std;

class Pessoa{
 
private:string nome;
        string telefone;
        string idade;
public: Pessoa();
		Pessoa(string nome,string telefone,string idade);
		string getIdade();		
		void setIdade(string idade);
		string getTelefone();
		void setTelefone(string telefone);
		string getNome();
		void setNome(string nome);

};
#endif

