all:		pessoa.o amigo.o contato.o main.o
		g++ -o main amigo.o contato.o  pessoa.o main.o

pessoa.o:	pessoa.cpp pessoa.hpp
		g++ -c pessoa.cpp
amigo.o:    amigo.cpp amigo.hpp
		g++ -c amigo.cpp
contato.o:	contato.cpp contato.hpp
		g++ -c contato.cpp
 
main.o:		main.cpp amigo.hpp contato.hpp pessoa.hpp
		g++ -c main.cpp

clean:
		rm -rf *.o

run:
		./main
