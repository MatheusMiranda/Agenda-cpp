#include <iostream>
#include "pessoa.hpp"
#include "amigo.hpp"
#include "contato.hpp"

using namespace std;

int main(){
    
    Pessoa umaPessoa;
	Pessoa outraPessoa; 
	amigo umamigo;
	contato umcontato;

	//cout << "Nome: " << umaPessoa.getNome() << endl;
	//cout << "Idade: " << umaPessoa.getIdade() << endl;
	//cout << "Telefone: " << umaPessoa.getTelefone() << endl;
	
	outraPessoa.setNome("Matheus");
	outraPessoa.setIdade("18");
	outraPessoa.setTelefone("3434-6771");
	
	cout << "Nome: " << outraPessoa.getNome() << endl;
    cout << "Idade: " << outraPessoa.getIdade() << endl;
	cout << "Telefone: " << outraPessoa.getTelefone() << endl;

	umamigo.setFacebook("matheusprogramador@lalala.com");
	umamigo.setEmail("matheusemail@lalala.com");
	
	cout<<	"Facebook: " <<umamigo.getFacebook() <<endl;
	cout<<	"Email: " <<umamigo.getEmail() <<endl;

	umcontato.setReferencia("conheço da Fga, a melhor faculdade do multiverso");

	cout<<	"Referencia: " <<umcontato.getReferencia() <<endl;



	return 0;
}
