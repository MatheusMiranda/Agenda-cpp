#ifndef AMIGO_H
#define AMIGO_H

#include "pessoa.hpp"
#include <string>


class amigo : public Pessoa{

private:string facebook;
        string email;
public:
		amigo();
		amigo(string nome,string idade, string telefone,string facebook,string email);
		string getFacebook();
		void setFacebook(string facebook);
		string getEmail();
		void setEmail(string email);

};
#endif